var rp = require('request-promise');

module.exports = function(config, is_test) {

	var statstore_url = config.statstore_url;
	var receiver_url = config.receiver_url;

	function prepare_queries(queries){
		if (queries.start === undefined) {
			queries.start = "2015-12-15T17:47:00";
		}

		if (queries.end === undefined) {
			queries.end = "2017-01-01T00:00:00";
		}
	} 

	var statstore = {
		get: {
			sum: function(queries) {

				prepare_queries(queries);

			    var options = {
					method: 'GET',
					uri: statstore_url + "/sum",
					qs: queries
				};

				return rp(options);
			},

			range: function(queries) {
			    
			    prepare_queries(queries);

			    var options = {
					method: 'GET',
					uri: statstore_url + '/range',
					qs: queries
				};

				return rp(options);
			}
		},

		
		post: function(data) {
			
		    var options = {
		    	method: 'POST',
		    	uri: receiver_url + '/log',
		    	body: data,
		    	json: true
		    };

		    return rp(options);

		}
	};

	var statstore_mocks = {
		get: {
			sum: function(queries) {
				return new Promise((resolve, reject) => {
					var json = {};

					// Variations so that we can test if we return an empty
					// object ('Tracker 2').
					switch (queries.key) {
						case "19b0fdd7-b452-4e4b-aba8-3c11fb2ac38e":
							json[queries.key] = 5;
							break;
						case "29b0fdd7-b452-4e4b-aba8-3c11fb2ac38e":
							break;
						default:
							json[queries.key] = 10;
					}

					resolve(JSON.stringify(json));
				});
			},

			range: function(queries) {
			    return new Promise((resolve, reject) => {
					var json = [];

					// Variations so that we can test if we return null
					// ('Tracker 2') or multiple objects in the array ('Tracker 1').
					switch (queries.key) {
						case "19b0fdd7-b452-4e4b-aba8-3c11fb2ac38e":
							json.push(
								{
									timestamp: "2016-02-12T12:00:00Z",
									value: 5
								},
								{
									timestamp: "2016-02-12T13:00:00Z",
									value: 0
								},
								{
									timestamp: "2016-02-12T14:00:00Z",
									value: 9
								},
								{
									timestamp: "2016-02-12T15:00:00Z",
									value: 12345
								},
								{
									timestamp: "2016-02-12T16:00:00Z",
									value: 1342
								}
							);
							break;
						case "29b0fdd7-b452-4e4b-aba8-3c11fb2ac38e":
							json = null;
							break;
						default:
							json.push({
								timestamp: "2016-02-12T12:00:00Z",
								value: 10
							});
					}

					resolve(JSON.stringify(json));
				});
			}
		},

		post: function(req, res, next) {
			return;
		}	
	};

	if (is_test) {
		return statstore_mocks;
	} else {
		return statstore;
	}
};